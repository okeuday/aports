# Contributor: Carlo Landmeter <clandmeter@alpinelinux.org>
# Maintainer: Carlo Landmeter <clandmeter@alpinelinux.org>
pkgname=c-ares
pkgver=1.20.1
pkgrel=0
pkgdesc="Asynchronous DNS/names resolver library"
url="https://c-ares.org/"
arch="all"
license="MIT"
subpackages="$pkgname-doc $pkgname-static $pkgname-dev $pkgname-utils::noarch"
source="https://c-ares.haxx.se/download/c-ares-$pkgver.tar.gz"

# secfixes:
#   1.17.2-r0:
#     - CVE-2021-3672

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--enable-shared
	make
}

check() {
	test/arestest --gtest_filter=-*.Live*
}

package() {
	make DESTDIR="$pkgdir" install
	cd src/tools
	install -Dm755 -t "$pkgdir"/usr/bin \
		adig ahost
}

utils() {
	amove usr/bin
}

sha512sums="
83400fb276ebcf16dfe6f43d56ca87839d132b5a0544420eda9fa148eb85468b3f215593fcefc2a7a3a8ed8b0d4ef093ed99616a4e466b01f6913934240539e4  c-ares-1.20.1.tar.gz
"
